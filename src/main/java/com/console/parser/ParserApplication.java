package com.console.parser;

import com.console.parser.entity.User;
import com.console.parser.service.ParseService;
import com.console.parser.service.ValidateArguments;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.List;
import java.util.function.Predicate;

@SpringBootApplication
public class ParserApplication implements CommandLineRunner {
    private ParseService parseService;
    private ValidateArguments validateArguments;

    public ParserApplication(ParseService parseService, ValidateArguments validateArguments) {
        this.parseService = parseService;
        this.validateArguments = validateArguments;
    }

    public static void main(String[] args) {
        SpringApplication.run(ParserApplication.class, args);
    }

    @Override
    public void run(String... args) {
        List<Predicate<User>> searchParameters = validateArguments.getSearchParameters(args);
        final List<User> parse = parseService.parse("user.xml", searchParameters);
        System.out.println(parse);
    }
}
