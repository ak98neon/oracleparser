package com.console.parser.factory;

import com.console.parser.entity.User;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.XMLEvent;

public class UserParseId implements UserParseElement {
    @Override
    public void parse(final XMLEventReader xmlEventReader, final User user) throws XMLStreamException {
        XMLEvent xmlEvent = xmlEventReader.nextEvent();
        user.setId(Long.valueOf(xmlEvent.asCharacters().getData()));
    }
}
