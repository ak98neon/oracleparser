package com.console.parser.factory;

import java.util.HashMap;
import java.util.Optional;

public class UserParseFactory {
    private static HashMap<String, UserParseElement> userParseElementHashMap = new HashMap<>();

    static {
        userParseElementHashMap.put("id", new UserParseId());
        userParseElementHashMap.put("name", new UserParseName());
        userParseElementHashMap.put("email", new UserParseEmail());
        userParseElementHashMap.put("age", new UserParseAge());
    }

    public static Optional<UserParseElement> getUserParseElement(String key) {
        return Optional.ofNullable(userParseElementHashMap.get(key));
    }
}
