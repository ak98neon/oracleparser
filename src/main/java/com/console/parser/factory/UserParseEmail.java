package com.console.parser.factory;

import com.console.parser.entity.User;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.XMLEvent;

public class UserParseEmail implements UserParseElement {
    @Override
    public void parse(XMLEventReader xmlEventReader, User user) throws XMLStreamException {
        XMLEvent xmlEvent = xmlEventReader.nextEvent();
        user.setEmail(xmlEvent.asCharacters().getData());
    }
}
