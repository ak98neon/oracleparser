package com.console.parser.service;

import com.console.parser.entity.User;

import java.util.List;
import java.util.function.Predicate;

/**
 * Interface for parsing xml file, and save in {@link User} entity.
 */
public interface ParseService {
    /**
     * <p>Parse xml file by file name</p>
     *
     * @param fileName file which contains xml document
     * @return List of users
     */
    List<User> parse(final String fileName);

    /**
     * <p>Parse xml file by file name, with any criteria for user (age, name, email, max-max age)</p>
     *
     * @param fileName file which contains xml document
     * @return List of users
     */
    List<User> parse(final String fileName, final List<Predicate<User>> filterAttributes);
}
