package com.console.parser.service;

import com.console.parser.entity.User;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

import static org.junit.Assert.assertArrayEquals;

@SpringBootTest
@RunWith(SpringRunner.class)
public class ParseServiceImplTest {
    @Autowired
    private ParseService parseService;
    private String testFileName = "user.xml";

    @Test
    public void parseTestUserFile() {
        final List<User> parse = parseService.parse(testFileName);
        final List<User> users = new ArrayList<>();
        users.add(new User(10L, "User One", "user@mail.com", 35));
        users.add(new User(20L, "User Two", "user2@mail.com", 45));
        assertArrayEquals(users.toArray(), parse.toArray());
    }

    @Test
    public void parseTestUserFileWithArgument() {
        final List<Predicate<User>> filterList = new ArrayList<>();
        final Predicate<User> checkAge = x -> x.getAge() > 20 && x.getAge() <= 35;
        filterList.add(checkAge);
        final List<User> parse = parseService.parse(testFileName, filterList);
        final List<User> users = new ArrayList<>();
        users.add(new User(10L, "User One", "user@mail.com", 35));
        assertArrayEquals(users.toArray(), parse.toArray());
    }
}
