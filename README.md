<h1>Instruction for run</h1>
<h5>All commands start in command line</h5>
<ul>
    <li>mvn clean</li>
    <li>mvn install</li>
    <li>java -jar target/parser-0.0.1-SNAPSHOT.jar</li>
    <li>Run with arguments java -jar target/parser-0.0.1-SNAPSHOT.jar minAge=20 maxAge=35 name=test email=test@email.com</li>
</ul>
<p>Arguments can be put together</p>